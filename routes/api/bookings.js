const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");
const Booking = require("../../models/Booking");
const User = require("../../models/User");

const nodemailer = require('nodemailer');

router.post("/book", auth, async (req, res) => {
  try {
    const book = new Booking({
      date_time: req.body.date_time,
      name: req.body.name,
      number: req.body.number,
      user: req.user.id
    });

    await book.save();

    res.json(book);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

router.get("/mybook", auth, async (req, res) => {
  try {
    const book = await Booking.find({ user: req.user.id }).populate('user', 'firstname lastname number', User);

    res.json(book);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

router.get("/allbook", auth, async (req, res) => {
  try {
    const book = await Booking.find({}).populate('user','firstname lastname number email', User);

    res.json(book);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

router.delete("/:id", auth, async (req, res) => {
  try {
    const book = await Booking.findOneAndRemove({ _id: req.params.id });

    res.json(book);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

router.put("/:id/rejected", auth, async (req, res) => {
  try {
    const book = await Booking.findOneAndUpdate({ _id: req.params.id },{status:'rejected'});

    res.json(book);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

router.put("/:id/accepted", auth, async (req, res) => {
  try {
    const book = await Booking.findOneAndUpdate({ _id: req.params.id },{status:'accepted'});



    const user = await Booking.findOne({ _id: req.params.id }).populate('user', 'email', User);

    nodemailer.createTestAccount((err, account) => {
      let transporter = nodemailer.createTransport({
        host: 'smtp.googlemail.com', // Gmail Host
        port: 465, // Port
        secure: true, // this is true as port is 465
        auth: {
          user: 'myriad.x.69@gmail.com', //Gmail username
          pass: 'myriad.sample.69' // Gmail password
        }
      });

      let mailOptions = {
        from: '"Myriad Support" <myriad.x.69@gmail.com>',
        to: user.user.email, // Recepient email address. Multiple emails can send separated by commas
        subject: 'Booking Confirmation',
        text: 'Please be advised that the booking request you made has been Rejected.'
      };

      transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          return console.log(error);
        }
        console.log('Message sent: %s', info.messageId);
      });
    });






    res.json(book);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});



module.exports = router;
