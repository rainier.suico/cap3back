const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const jwtSecret = require('../../config/keys').jwtSecret;
const User = require('../../models/User');
const { check, validationResult } = require('express-validator');

// @route POST api/users/register
// @desc Register user
// @access Public
router.post(
  '/register',
  [
    check('username')
      .not()
      .isEmpty()
      .withMessage('Username field is required')
      .isLength({min: 6})
      .withMessage('Username must be at least 6 characters'),
    check('firstname', 'First Name field is required')
      .not()
      .isEmpty(),
    check('lastname', 'Last Name field is required')
      .not()
      .isEmpty(),
    check('number', 'Phone number field is required')
      .not()
      .isEmpty(),
    check('email')
      .not()
      .isEmpty()
      .withMessage('Email field is required')
      .isEmail()
      .withMessage('Email is invalid'),
    check('password', 'Password must be at least 8 characters').isLength({
      min: 8
    }),
    check('password2')
      .not()
      .isEmpty()
      .withMessage('Confirm Password field is required')
      .custom((value, { req, loc, path }) => {
        if (value !== req.body.password) {
          throw new Error('Passwords must match');
        } else {
          return value;
        }
      })
  ],
  async (req, res) => {
    let errors = validationResult(req);
    if (!errors.isEmpty()) {
      var errorResponse = errors.array({ onlyFirstError: true });

      const extractedErrors = {};
      errorResponse.map(err => (extractedErrors[err.param] = err.msg));
      return res.status(400).json(extractedErrors);
    }

    const { username, firstname, lastname, number, email, password } = req.body;

    try {
      let user = await User.findOne({ username });

      if (user) {
        return res.status(400).json({ username: 'Username already exists' });
      }

      user = await User.findOne({ email });

      if (user) {
        return res.status(400).json({ email: 'Email already exists' });
      }

      user = new User({
        username,
        firstname,
        lastname,
        number,
        email,
        password
      });

      const salt = await bcrypt.genSalt(10);

      user.password = await bcrypt.hash(password, salt);

      const newUser = await user.save();
      res.json(newUser);
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

// @route POST api/users/login
// @desc Login user and return JWT token
// @access Public
router.post(
  '/login',
  [
    check('username')
      .not()
      .isEmpty()
      .withMessage('Username field is required')
      .withMessage('Username is invalid'),
    check('password')
      .not()
      .isEmpty()
      .withMessage('Password field is required')
  ],
  async (req, res) => {
    let errors = validationResult(req);
    if (!errors.isEmpty()) {
      var errorResponse = errors.array({ onlyFirstError: true });

      const extractedErrors = {};
      errorResponse.map(err => (extractedErrors[err.param] = err.msg));
      return res.status(400).json(extractedErrors);
    }

    const { username, password } = req.body;

    try {
      let user = await User.findOne({ username });

      if (!user) {
        return res.status(400).json({ username: 'Username not found' });
      }

      const isMatch = await bcrypt.compare(password, user.password);

      if (!isMatch) {
        return res
          .status(400)
          .json({ password: 'Password incorrect' });
      }

      const payload = {
        id: user.id,
        username: user.username,
        firstname: user.firstname,
        lastname: user.lastname,
        number: user.number,
        email: user.email
      };

      jwt.sign(
        payload,
        jwtSecret,
        {
          expiresIn: 360000
        },
        (err, token) => {
          if (err) throw err;
          res.json({ success: true, token });
        }
      );
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error');
    }
  }
);

module.exports = router;
